var game = new Phaser.Game(800,600,Phaser.CANVAS,'gameDiv');

var backg1;
var backg0;
var backg2;
var backg3;
var roadLayer;
var rightKey;
var leftKey;
var flash;
var button;
var carSprite;
var integer = 1;
var upKey;


var mainState = {
	preload:function(){
		this.load.image('OFF',"assets/flashoff.png");
		this.load.image('ON',"assets/flashon.png");
    this.load.image('bg1',"assets/bg.png");
    this.load.image('bg2',"assets/cityscape.png");
    this.load.image('bg0',"assets/city.png");
    this.load.image('bg3',"assets/clouds.png");
    this.load.image('cloudnight',"assets/cloudsnight.png");
    this.load.image('road',"assets/road.png");
    this.load.image('carright',"assets/carright.png");
    this.load.image('carflashonleft',"assets/carflashon.png");
    this.load.image('carflashonright',"assets/carflashonright.png");
this.load.image('carleft',"assets/car.png");
	},

	create:function(){
		 game.physics.startSystem(Phaser.Physics.ARCADE);


  ball = game.add.sprite(50, 50, 'ball1');
    game.physics.enable(ball, Phaser.Physics.ARCADE);
    ball.body.velocity.set(150, 150);




    //bg = game.add.tileSprite(0, 0, 800, 600, 'background');



    game.physics.arcade.gravity.y = 250;



		backg3 = this.add.tileSprite(0,0,800,600,'bg3');
		backg1 = this.add.tileSprite(0,0,800,600,'bg1');
		backg2 = this.add.tileSprite(0,0,800,600,'bg2');
		roadLayer = this.add.tileSprite(0,0,800,600,'road');
			backg0 = this.add.tileSprite(0,0,800,200,'bg0');
 carSprite = this.add.sprite(400,480,'carright');
			carSprite.anchor.setTo(.5, 0); 
   // player = game.add.sprite(32, 32, 'dude');

    game.physics.enable(carSprite, Phaser.Physics.ARCADE);



    carSprite.body.bounce.y = 0.2;

    carSprite.body.collideWorldBounds = true;
 button = game.add.button(game.world.centerX - 385, 10, 'OFF', actionOnClick, this, 2, 1, 0);
			
		
flash = game.input.keyboard.addKey(Phaser.KeyCode.F);
	rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
	leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
	},

	update:function(){
		carSprite.body.velocity.x =0;

if (leftKey.isDown)

    {

carSprite.body.velocity.x = -100;		
backg1.tilePosition.x += 1;
backg0.tilePosition.x += 3;
backg2.tilePosition.x += 2;
backg3.tilePosition.x += 0.5;
roadLayer.tilePosition.x +=6;

} else if(rightKey.isDown){

roadLayer.tilePosition.x -= 6;
backg1.tilePosition.x -= 1;
backg0.tilePosition.x -= 3;
backg2.tilePosition.x -= 2;
backg3.tilePosition.x -= 0.5;
carSprite.body.velocity.x = 100;
}
else if(carSprite.position.y==510)
	{if(upKey.isDown){
 carSprite.body.velocity.y = -250;

        jumpTimer = game.time.now + 750;}else{}}
	if(integer){
button.loadTexture('ON');
		carSprite.loadTexture('carflashonright');
		backg3.loadTexture('cloudnight');
	}else{
		backg3.loadTexture('bg3');
		button.loadTexture('OFF');
	carSprite.loadTexture('carright');
}
	},


}
function actionOnClick(){
	integer=!integer;
}
game.state.add('mainState',mainState);
game.state.start('mainState');